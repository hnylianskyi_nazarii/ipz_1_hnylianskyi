import http from 'http';
const PORT = 8000;
import { database } from './firebase.js';

const server = http.createServer(function (req, res) {
    let reqBody = '';


    req.on('data', (chunk) => {
        reqBody += chunk;
    })

    req.on('end', async () => {
        let resMessage = '';
        reqBody = decodeURI(reqBody);
        let reqBodyArr = reqBody.split('+');
        console.log(reqBodyArr);

        let task = reqBodyArr[0].replace('data=', '');
        if (task === 'createUser') {
            resMessage = await createUser(reqBodyArr[2], reqBodyArr[4], reqBodyArr[6]);
        }
        else if (task === 'logIn') {
            resMessage = await logIn(reqBodyArr[2], reqBodyArr[4]);
        }
        else if (task == 'getAllProducts') {
            resMessage = await getAllProducts();
        }
        else if (task == 'addProduct') {
            resMessage = await addProduct(reqBodyArr[2], reqBodyArr[4], reqBodyArr[6]);
        }
        else if (task == 'logOut') {
            resMessage = await logOut(reqBodyArr[2]);
        }
        else if (task == 'searchById' || task == 'searchByName') {
            resMessage = await searchProduct(reqBodyArr[2]);
        }
        else if (task == 'editProduct') {
            resMessage = await editProduct(reqBodyArr[2], reqBodyArr[4]);
        }

        console.log(`Відповідь сервера: ${resMessage}`);
        res.end(resMessage);
    })
});

server.listen(PORT, (error) => {
    error ? console.log(error) : console.log(`Listening on ${PORT}`);
});



async function createUser(name, email, password) {
    let message = '';
    email = email.replace('%40', '@');
    email = email.replace(/\./g, '+');
    await database.ref(`users/${email}`).get().then(async snapshot => {
        if (snapshot.exists()) {
            message = 'false,Користувач з такою поштою вже зареєстрований!';
        }
        else {
            await database.ref('users/' + email).set({
                name,
                password,
                "loginNow": true
            });
            message = 'true,Реєстрація пройшла успішно!';
        }
    });
    return message;
}

async function logIn(email, password) {
    let message = '';
    email = email.replace('%40', '@').replace(/\./g, '+');
    await database.ref().child(`users/${email}`).get().then((snapshot) => {
        if (snapshot.exists()) {
            const user = snapshot.val();
            if (password !== user.password) {
                message = 'false,Невірний пароль';
            }
            else if (user.loginNow) {
                message = 'false,Ви не можете ввійти оскільки\nкористувач з таким логіном зараз перебуває у системі';
            }
            else {
                let updates = user;
                updates['loginNow'] = true;
                database.ref().child(`users/${email}`).update(updates);
                message = `true,${user.name}`;
            }
        } else {
            message = 'false,Невірна пошта';
        }
    }).catch((error) => {
        console.error(error);
        message = 'false,Помилка з базою даних';
    });
    return message;
}


async function logOut(email) {
    let message = '';
    email = email.replace('%40', '@').replace(/\./g, '+');
    console.log(email);
    let updates = {};
    await database.ref(`users/${email}`).get().then(async snapshot => {
        if (snapshot.exists()) {
            updates = snapshot.val();
        }
        else {
            message = 'false,Помилка з базою даних, спробуйте виконати вхід пізніше';
        }
    }).catch(error => {
        message = 'false,Помилка з базою даних, спробуйте виконати вхід пізніше';
        console.log(error);
    })

    if (Object.keys(updates).length !== 0) {
        updates['loginNow'] = false;
        await database.ref(`users/${email}`).update(updates);
        message = 'true,Можете виконати вхід';
    }
    return message;
}

async function getAllProducts() {
    let message = 'true,';
    await database.ref("products/").once('value', (snapshot) => {
        if (snapshot.exists()) {
            snapshot.forEach((childSnapshot) => {
                let childKey = childSnapshot.key;
                let childData = childSnapshot.val();
                message += `${childKey} ${childData.name} ${childData.count}\n`;
            });
        }

    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    });

    return message;
}


async function addProduct(id, name, count) {
    let message = '';
    name = name.replace(/\./g, ' ');
    await database.ref(`products/${id}`).get().then(async snapshot => {
        if (snapshot.exists()) {
            message = 'false, Товар з таким id вже існує!';
        }
        else {
            await database.ref(`products/${id}`).set({
                name,
                count,
            });
            message = 'true,Товар успішно добавлений!';
        }
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    });
    return message;
}

async function searchProduct(value) {
    let message = '';
    let productsListObj = {};
    await database.ref("products/").once('value', (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            var childKey = childSnapshot.key;
            var childData = childSnapshot.val();
            productsListObj[childKey] = childData;
        });
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    });

    ptr1: for (let key in productsListObj) {
        if (/\D/g.test(value)) {
            if (productsListObj[key].name === value) {
                message = `true,${key} ${productsListObj[key].name} ${productsListObj[key].count}`;
                break ptr1;
            }
        }
        else {
            if (key === value) {
                message = `true,${key} ${productsListObj[key].name} ${productsListObj[key].count}`;
                break ptr1;
            }
        }
    }
    return message ? message : 'false,Товар не знайдено';
}

async function editProduct(id, count) {
    let message = '';
    await database.ref(`products/${id}`).get().then(async snapshot => {
        if (snapshot.exists()) {
            let updates = {};
            updates[`/count`] = count;
            message = 'true, Товар успішно редаговано!';
            await database.ref(`products/${id}`).update(updates);
        }
    }).catch(error => {
        message = 'false,Помилка з базою даних';
        console.log(error);
    });
    return message;
}