import firebase from "firebase/compat/app";
import "firebase/compat/database";

const firebaseConfig = {
  apiKey: "AIzaSyA7MRliLUDDpXoq5uKe3PYOr3_mmOtbpaA",
  authDomain: "storage-49629.firebaseapp.com",
  projectId: "storage-49629",
  storageBucket: "storage-49629.appspot.com",
  messagingSenderId: "890704779333",
  appId: "1:890704779333:web:c9eea298408a6b4dd9ab2c"
};

firebase.default.initializeApp(firebaseConfig);

export const database = firebase.database();


